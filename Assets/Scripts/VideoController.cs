﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;

public class VideoController : MonoBehaviour
{
    [SerializeField] Button[] playButton;
    [SerializeField] GameObject[] infos;

    public Button[] buttons;

    private VideoPlayer videoPlayer;
    

    // The components are called in Awake
    void Awake()
    {
        videoPlayer = GetComponent<VideoPlayer>();
        StartCoroutine(FirstInteraction(17.7f));
    }

    // This is were the different playButtons are activated when two infos[] are active
    void Update()
    {
        if(infos[0].activeInHierarchy == true)
        {
            playButton[0].gameObject.SetActive(true);
        }

        if (infos[1].activeInHierarchy == true && infos[2].activeInHierarchy == true)
        {
            playButton[1].gameObject.SetActive(true);
        }

        if (infos[3].activeInHierarchy == true)
        {
            playButton[2].gameObject.SetActive(true);
        }

        if (infos[4].activeInHierarchy == true)
        {
            playButton[3].gameObject.SetActive(true);
        }

        if (infos[5].activeInHierarchy == true)
        {
            playButton[4].gameObject.SetActive(true);
        }

        if (infos[6].activeInHierarchy == true)
        {
            playButton[5].gameObject.SetActive(true);
        }

        if (infos[7].activeInHierarchy == true && infos[8].activeInHierarchy == true)
        {
            playButton[6].gameObject.SetActive(true);
        }
    }
    //Creates a coroutine for the first part of the video before it is paused
    private IEnumerator FirstInteraction(float waitTime)
    {
        Debug.Log("First sections starts");
        yield return new WaitForSeconds(waitTime);
        videoPlayer.Pause();
        buttons[0].gameObject.SetActive(true);
    }

    //Creates a coroutine for the second part of the video before it is paused
    private IEnumerator SecondInteraction(float waitTime)
    {
        Debug.Log("Second section starts");
        yield return new WaitForSeconds(waitTime);
        videoPlayer.Pause();
        Debug.Log("Video paused");
        buttons[1].gameObject.SetActive(true);
        buttons[2].gameObject.SetActive(true);
    }

    //Creates a coroutine for the third part of the video before it is paused
    private IEnumerator ThirdInteraction(float waitTime)
    {
        Debug.Log("Third section starts");
        yield return new WaitForSeconds(waitTime);
        videoPlayer.Pause();
        Debug.Log("Video paused");
        buttons[3].gameObject.SetActive(true);
    }

    //Creates a coroutine for the third part of the video before it is paused
    private IEnumerator FourthInteraction(float waitTime)
    {
        Debug.Log("Fourth section starts");
        yield return new WaitForSeconds(waitTime);
        videoPlayer.Pause();
        Debug.Log("Video paused");
        buttons[4].gameObject.SetActive(true);
    }

    //Creates a coroutine for the fourth part of the video before it is paused
    private IEnumerator FifthInteraction(float waitTime)
    {
        Debug.Log("Fifth section starts");
        yield return new WaitForSeconds(waitTime);
        videoPlayer.Pause();
        Debug.Log("Video paused");
        buttons[5].gameObject.SetActive(true);
    }

    //Creates a coroutine for the fifth part of the video before it is paused
    private IEnumerator SixthInteraction(float waitTime)
    {
        Debug.Log("Sixth section starts");
        yield return new WaitForSeconds(waitTime);
        videoPlayer.Pause();
        Debug.Log("Video paused");
        buttons[6].gameObject.SetActive(true);
    }

    //Creates a coroutine for the sixth part of the video before it is paused
    private IEnumerator SeventhInteraction(float waitTime)
    {
        Debug.Log("Seventh section starts");
        yield return new WaitForSeconds(waitTime);
        videoPlayer.Pause();
        Debug.Log("Video paused");
        buttons[7].gameObject.SetActive(true);
        buttons[8].gameObject.SetActive(true);
    }

    public void PlayVideo()
    {
        videoPlayer.Play();
        StartCoroutine(SecondInteraction(5));
        buttons[0].gameObject.SetActive(false);
        infos[0].gameObject.SetActive(false);
        playButton[0].gameObject.SetActive(false);
        Debug.Log("First section is over");
    }
    public void PlayVideoTwo()
    {
        videoPlayer.Play();
        StartCoroutine(ThirdInteraction(12));
        buttons[1].gameObject.SetActive(false);
        buttons[2].gameObject.SetActive(false);
        infos[1].gameObject.SetActive(false);
        infos[2].gameObject.SetActive(false);
        playButton[1].gameObject.SetActive(false);
        Debug.Log("Second section is over");
    }
    public void PlayVideoThree()
    {
        videoPlayer.Play();
        StartCoroutine(FourthInteraction(18));
        buttons[3].gameObject.SetActive(false);
        infos[3].gameObject.SetActive(false);
        playButton[2].gameObject.SetActive(false);
        Debug.Log("Third section is over");
    }
    public void PlayVideoFour()
    {
        videoPlayer.Play();
        StartCoroutine(FifthInteraction(23.5f));
        buttons[4].gameObject.SetActive(false);
        infos[4].gameObject.SetActive(false);
        playButton[3].gameObject.SetActive(false);
        Debug.Log("Fourth section is over");
    }
    public void PlayVideoFive()
    {
        videoPlayer.Play();
        StartCoroutine(SixthInteraction(7.0f));
        buttons[5].gameObject.SetActive(false);
        infos[5].gameObject.SetActive(false);
        playButton[4].gameObject.SetActive(false);
        Debug.Log("Fifth section is over");
    }
    public void PlayVideoSix()
    {
        videoPlayer.Play();
        StartCoroutine(SeventhInteraction(13.1f));
        buttons[6].gameObject.SetActive(false);
        infos[6].gameObject.SetActive(false);
        playButton[5].gameObject.SetActive(false);
        Debug.Log("Sixth section is over");
    }
    public void PlayVideoSeven()
    {
        videoPlayer.Play();
        buttons[7].gameObject.SetActive(false);
        buttons[8].gameObject.SetActive(false);
        infos[7].gameObject.SetActive(false);
        infos[8].gameObject.SetActive(false);
        playButton[6].gameObject.SetActive(false);
        Debug.Log("Seventh section is over");
    }
    public void PlayVideoEight()
    {
        videoPlayer.Play();
        Debug.Log("Eight section is over");
    }

}
